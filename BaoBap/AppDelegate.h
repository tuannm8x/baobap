//
//  AppDelegate.h
//  BaoBap
//
//  Created by meo mun on 6/10/16.
//  Copyright © 2016 meo mun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

