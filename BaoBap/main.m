//
//  main.m
//  BaoBap
//
//  Created by meo mun on 6/10/16.
//  Copyright © 2016 meo mun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
